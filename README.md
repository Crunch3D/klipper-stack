# Klipper stack

A super simple Klipper setup for desktop Linux PCs, with minimal permissions (i.e. only for the Mainsail docker option).

Included are some configuration files to get everything up and running as fast as possible. Just run the above, and then a few more command sets below.

The secondary objective of this "meta" repository is to include all modifications of the Klipper stack, to finally achieve "valid state" repositories in Moonraker, which are then updatable through Mainsail:

![ms_updates.png](./docs/imgs/ms_updates.png)

This repository uses git submodules to include the following **modified versions** of the following **original** projects:

- [Klipper](https://www.klipper3d.org/)
- [Moonraker](https://moonraker.readthedocs.io/en/latest/)
- [Mainsail](https://docs.mainsail.xyz/)
- [Simulavr](https://www.nongnu.org/simulavr/) (for testing without hardware)
- Configuration files.

To use your own forks, fork this repository, and update the remotes in the [.gitmodules](.gitmodules) file, which must contain something like the following.

```
[submodule "klipper"]
	path = klipper
	url = https://gitlab.com/pipettin-bot/forks/klipper.git
[submodule "moonraker"]
	path = moonraker
	url = https://gitlab.com/pipettin-bot/forks/moonraker.git
[submodule "mainsail"]
	path = mainsail
	url = https://gitlab.com/pipettin-bot/forks/mainsail.git
[submodule "simulavr"]
	path = simulavr
	url = git://git.savannah.nongnu.org/simulavr.git
```

# Install

You'll need `git` installed on your system:

```bash
# Arch Linux
sudo pacman -Syu git

# Ubuntu/Raspi OS
sudo apt update
sudo apt upgrade -y
sudo apt install -y git
```

## Download components

Download stuff!

```bash
# Clone the meta-repo, without initializing the submodules.
git clone https://gitlab.com/pipettin-bot/forks/klipper-stack.git
# Then change to the repo's directory.
cd klipper-stack
```

Unless noted, all commands are meant to be run from the `klipper-stack` directory.

```bash
# Use a bash onliner to clone the default remote branch of the submodules as nested git repos.
# This is needed only because Moonraker does not recongnize submodules as valid repos. :shrug:
# Any deviation from the shape of the .gitmodules file above is on you.
git config --file .gitmodules --get-regexp "path|url" | awk '{ print $2 }' | paste - - | awk '{ system("git clone "$2" "$1) }'
```

If you do not plan on updating nor managing the processes through Mainsail, simply run:

```bash
# Download everything including submodules.
git clone --recurse-submodules -j8 https://gitlab.com/pipettin-bot/forks/klipper-stack.git
cd klipper-stack
# Auto-update submodules on every git pull.
git config submodule.recurse true
```

Then use the good old `git pull` command to update everything at any time.

## Requirements

Klipper: the "scripts" directory in the Klipper repository has dependencies for many Linux distributions.

### System dependencies

Arch Linux:

```bash
sudo pacman -S libffi base-devel ncurses libusb
sudo pacman -S avrdude avr-gcc avr-binutils avr-libc arm-none-eabi-newlib arm-none-eabi-gcc arm-none-eabi-binutils
```

Ubuntu Linux / Raspberry Pi OS for Klipper and Moonraker:

```bash
# System
sudo apt install -y python3 python3-venv
# Klipper
sudo apt install -y virtualenv python3-dev libffi-dev build-essential libncurses-dev libusb-dev
sudo apt install -y avrdude gcc-avr binutils-avr avr-libc stm32flash dfu-util libnewlib-arm-none-eabi gcc-arm-none-eabi binutils-arm-none-eabi libusb-1.0-0 
# Moonraker
sudo apt install -y python3-virtualenv python3-dev python3-libgpiod liblmdb-dev libopenjp2-7 libsodium-dev zlib1g-dev libjpeg-dev packagekit wireless-tools curl
```

If you are running a slightly old version of Ubuntu or Raspberry Pi OS, you may need to choose a particular version of the "avr-libc avrdude binutils-avr gcc-avr" packages. See the troubleshooting section "Klipper not flashing the Arduino UNO" below for instructions.

### Python dependencies and environments

Klippy:

```bash
python3 -m venv klippy-env && \
source klippy-env/bin/activate && \
pip install -r klipper/scripts/klippy-requirements.txt && \
deactivate
```

Moonraker:

```bash
python3 -m venv moonraker-env && \
source moonraker-env/bin/activate && \
pip install -r moonraker/scripts/moonraker-requirements.txt && \
deactivate
```

## Mainsail development server

Official instructions at: https://docs.mainsail.xyz/overview/developement/environment

Skip/replace this part of the instructions if you'd rather use the docker setup or the "regular" `nginx` setup. Instructions for those two are at the "Mainsail" section (further down in this file).

Install dependencies:

```bash
# Arch Linux
sudo pacman -S npm node

# Debian/Ubuntu
sudo apt install npm node
```

Setup the development environment file:

```bash
cd mainsail
cp .env.development.local.example .env.development.local
```

Edit `.env.development.local` and update it if required. Options described [here](https://docs.mainsail.xyz/overview/developement/environment) and in the file. For example:

```
# hostname or ip from the moonraker instance
VUE_APP_HOSTNAME=localhost

# port from the moonraker instance
VUE_APP_PORT=7125

# reconnect interval in ms
VUE_APP_RECONNECT_INTERVAL=5000

# where should mainsail read the instances from (moonraker, browser or json)
VUE_APP_INSTANCES_DB="moonraker"

# defaults for multi language tests
VUE_APP_I18N_LOCALE=en
VUE_APP_I18N_FALLBACK_LOCALE=en
```

Install node packages:

```bash
npm install
```

All you need to do to run the app from now on is to run from `klipper-stack/mainsail`:

```bash
npm run serve
```

Then visit [http://localhost:8080](http://localhost:8080) to open Mainsail.

### Configuration

You can adjust the port at which the development server runs by editing the `vite.config.ts` file.

```json
    server: {
        host: '0.0.0.0',
        port: 8080,
    },
```

You can adjust `klipper-stack/mainsail/public/config.json` to point Mainsail to the local Moonraker instance, for example:

```json
{
    "defaultLocale": "en",
    "hostname": null,
    "port": null,
    "instancesDB": "json",
    "instances": [
        { "hostname": "localhost", "port": 7125 }
    ]
}
```

# Moonraker configuration

Edit the [klipper-stack/printer_data/config/moonraker.conf](./printer_data/config/moonraker.conf) file to your liking.

The configuration reference is here: https://moonraker.readthedocs.io/en/latest/configuration/

Example contents:

```
# Moonraker Configuration File
[server]
host: 0.0.0.0
port: 7125
klippy_uds_address: /tmp/klippy_uds

[machine]
provider: systemd_cli
# https://moonraker.readthedocs.io/en/latest/configuration/#machine
validate_service: False

[authorization]
cors_domains:
    https://my.mainsail.xyz
    http://my.mainsail.xyz
    http://*.local
    http://*.lan
    http://localhost
    *//localhost:8080
trusted_clients:
    10.0.0.0/8
    127.0.0.0/8
    169.254.0.0/16
    172.16.0.0/12
    192.168.0.0/16
    FE80::/10
    ::1/128
    172.17.0.0/16

[update_manager]
enable_auto_refresh: False
refresh_interval: 672
enable_system_updates: False
channel: dev

[update_manager klipper]
type: git_repo
channel: dev
path: /path/to/your/klipper-stack/klipper
origin: https://gitlab.com/pipettin-bot/forks/klipper.git
primary_branch: pipetting
host_repo: https://gitlab.com/pipettin-bot/forks/klipper
is_system_service: True
managed_services: klipper
```

> Caveat: Moonraker will prevent editing any configuration files from Mainsail if the klipper-stack repo is registered as an updatable module. This is because the repository _contains_ the `printer_data` directory.

# Klipper configuration

Follow the "official" docs for flashing the firmware and finding your devices: https://www.klipper3d.org/Installation.html

Cloned Arduinos may share IDs, so using the "by path" strategy to find the serial port will work when the "by id" listing might not. Here is the "by path" command and an example output, and compared to the regular "by id" command:

```bash
ls /dev/serial/by-path/*
# /dev/serial/by-path/platform-3f980000.usb-usb-0:1.4:1.0-port0  /dev/serial/by-path/platform-3f980000.usb-usb-0:1.5:1.0

ls /dev/serial/by-id/*
# /dev/serial/by-id/usb-1a86_USB_Serial-if00-port0  /dev/serial/by-id/usb-Arduino__www.arduino.cc__0043_85334343738351B00140-if00
```

If you already have your control boards plugged to your Klipper "host" computer, run the commands and save the output. You will need it to update the configuration files to point the MCU definitions, replacing the default paths with the ones you obtained with the above commands.

Several Klipper configuration "profiles" can be activated in the main [printer.cfg](./printer_data/config/printer.cfg) file, including the one for the `simulavr` setup described here. To do that, open the file, uncomment one of the include sections (i.e. remove the leading `#`), and comment-out the rest.

For example, you may uncomment the `[include printer_minimal.cfg]` line, comment the rest, and edit the [printer_minimal.cfg](./printer_data/config/printer_minimal.cfg) file to look something like this:

```
[mcu]
serial: /dev/serial/by-path/platform-3f980000.usb-usb-0:1.5:1.0

[mcu tools]
# Secondary MCU
serial: /dev/serial/by-path/platform-3f980000.usb-usb-0:1.4:1.0-port0

[printer]
kinematics: none
max_velocity: 1
max_accel: 1
```

This configuration is the simplest and safest to test out the software stack.

There are a few bundled configuration profiles:

- [printer_minimal.cfg](./printer_data/config/printer_minimal.cfg): Barebones config, only requires a serial path.
- Configuration for `simulavr`, this is the **default**: [printer.cfg](./printer_data/config/simulavr/printer.cfg).
- Several configurations for the Arduino CNC-shield:
    - [cnc_shield_manual_stepper](./printer_data/config/cnc_shield_manual_stepper): manual-stepper-only configuration (no XYZE steppers).
    - [cnc_shield_xyz_extrath_a](./printer_data/config/cnc_shield_xyz_extrath_a): secondary toolhead configuration (experimental).
    - [cnc_shield_xyza](./printer_data/config/cnc_shield_xyza): fourth axis configuration.
    - [cnc_shield_xyze](./printer_data/config/cnc_shield_xyze): regular configuration.
    - [pin_aliases](./printer_data/config/pin_aliases): Arduino CNC-shield pin aliases (v3.0 clone).
- A configuration file for the Pi Pico: [printer.cfg](./printer_data/config/pi_pico/printer.cfg).
- All of the [reference configuration files](./klipper/config) from the Klipper repository.

You can edit these files through SSH or Mainsail.

# Simulavr setup

The simulavr way is materially the cheapest way to run Klipper. The default config file points to this "MCU".

Summarized instructions follow.

Original instructions here: https://www.klipper3d.org/Debugging.html?h=simulav#testing-with-simulavr

## Dependencies

```bash
# Install deps for simulavr (Arch).
sudo pacman -S cmake swig time help2man python-docutils valgrind

# Install deps for simulavr (Ubuntu).
sudo apt install cmake swig time help2man python3-docutils valgrind
```

## Build simulavr

The following assumes that the current directory is "klipper-stack" (i.e. to the parent directory of moonraker, klipper, mainsail, etc.).

```bash
# Build simulavr
cd simulavr && \
make python && \
make build
```

## Build klipper

The following assumes that the current directory is "klipper-stack" (i.e. to the parent directory of moonraker, klipper, mainsail, etc.).

```bash
# Configure Klipper for simulavr.
source klippy-env/bin/activate && \
cd klipper && \
make menuconfig  # This will open a "graphical" menu.
```

On the graphical menu that appears:

- Select the AVR architechture, and "atmega644p" as the processor model.
- Toggle "enable extra ..." at the top, new options will appear below.
- Then toggle the "compile for simulavr ..." and save.

![menuconfig_simulavr.png](./docs/imgs/menuconfig_simulavr.png)

Compile Klipper for the simulator:

```bash
make # me a sandwich
```

Check that the simulator works by starting it manually:

```bash
cd .. # This should take you to the klipper-stack directory.
PYTHONPATH=simulavr/build/pysimulavr/ ./klipper/scripts/avrsim.py klipper/out/klipper.elf
```

Which should print:

```
Starting AVR simulation: machine=atmega644 speed=16000000
Serial: port=/tmp/pseudoserial baud=250000
...
```

Hit `Ctrl+C` to stop the simulator, and deactivate the klippy enviroment:

```bash
deactivate
```

## Final details

Allow Moonraker to restart the simulavr service:

> Moonraker is not permitted to restart service 'simulavr'. To enable management of this service add simulavr to the bottom of the file `/path/to/klipper-stack/printer_data/moonraker.asvc`. To disable management for this service set `is_system_service: False` in the configuration for this section.

# Auto-start components with systemd

## User units

A few prototype systemd units need minor modifications to run as unprivileged "user" units (i.e. without root):

- [klipper.service](./klipper.service): derived from the Klipper install [scripts](https://github.com/Klipper3d/klipper/blob/master/scripts/).
- [moonraker.service](./moonraker.service): derived from the Moonraker install [scripts](https://github.com/Arksine/moonraker/tree/master/scripts).
- [simulavr.service](./simulavr.service): starts `simulavr`.
- [mainsail.service](./mainsail.service): uses NPM to run a development server without root privileges.
- [klipper.target](./klipper.target): a systemd "target" useful for (re)starting everything with a single command (read on).

If you'd like to start these units on boot, without logging in, you may need to [enable systemd "lingering"](https://wiki.archlinux.org/title/systemd/User#Automatic_start-up_of_systemd_user_instances).

If at any time you need to inspect the systemd logs, you may also use `journalctl --user -xeu moonraker.service -f` (replacing `moonraker.service` with the name of the service of interest).

You can always run the programs without systemd, using regular commands (last section of the file).

## System units

The Mainsail docker needs root though:

- [mainsail.docker.service](./mainsail.docker.service): generic Docker service, must be installed as a root service.

## Edit units

Edit those files, such that the `KLIPPER_STACK` variable points to the path where _you_ downloaded the klipper-stack repository (or the containing folder of each program). Otherise they will be flagged as "bad" by systemd, and be unavailable, or fail to start otherwise.

Running all units as root will make them easier to manage from Mainsail. If you want this, it will be convenient to uncomment the `User` field in each file (e.i. remove the `#`). The user will be `pi` for a raspberry pi, replace it with your username if running on a desktop Linux PC.

## Install units

### As user units

User unit definitions reside at `~/.config/systemd/user/`.

You need to edit the units before installing them, as noted above.  For example, the `klipper.service` file at `~/.config/systemd/user/` on a Raspberry Pi should look like this:

```
[Unit]
Description=Starts klipper on startup
PartOf=klipper.target

[Install]
WantedBy=multi-user.target

[Service]
Type=simple
RemainAfterExit=yes
Environment=KLIPPER_STACK=/home/pi/klipper-stack/
ExecStart=/bin/sh -c "${KLIPPER_STACK}/klippy-env/bin/python \
                      ${KLIPPER_STACK}/klipper/klippy/klippy.py \
                      ${KLIPPER_STACK}/printer_data/config/printer.cfg \
                      -l /tmp/klippy.log -a /tmp/klippy_uds"
```

Create the directory, link the units, and reload the systemd user daemon:

```bash
# Create the directory if it does not exist.
mkdir -p ~/.config/systemd/user/

# Copy the units (AFTER EDITING THEM).
cp klipper.service ~/.config/systemd/user/
cp moonraker.service ~/.config/systemd/user/
cp simulavr.service ~/.config/systemd/user/  # Only if used
cp mainsail.service ~/.config/systemd/user/  # Only if used

# Copy the target, grouping all units.
cp klipper.target ~/.config/systemd/user/

# Reload the user units
systemctl --user daemon-reload
```

Check the status of the units, they should be inactive:

```bash
# Start, stop, and restart all services using the target.
systemctl --user status klipper.target
# Check status of units in the target.
systemctl --user list-units --all --with-dependencies klipper.target
```

### As system units

If you want to use system-level units, as regular Klipper does, then copy the units over to `/etc/systemd/system/` instead.

In the `systemctl` commands that follow, use `sudo` throughout, and remove the `--user` flag.

You also have to edit them to enable the `User` setting. For example, the `klipper.service` file at `/etc/systemd/system/` on a Raspberry Pi should look like this:

```
[Unit]
Description=Starts klipper on startup
PartOf=klipper.target

[Install]
WantedBy=multi-user.target

[Service]
Type=simple
RemainAfterExit=yes
User=pi
Environment=KLIPPER_STACK=/home/pi/klipper-stack/
ExecStart=/bin/sh -c "${KLIPPER_STACK}/klippy-env/bin/python \
                      ${KLIPPER_STACK}/klipper/klippy/klippy.py \
                      ${KLIPPER_STACK}/printer_data/config/printer.cfg \
                      -l /tmp/klippy.log -a /tmp/klippy_uds"
```

To install the Klipper and Moonraker units, run:

```bash
# Copy the units (AFTER EDITING THEM).
sudo cp klipper.service /etc/systemd/system/
sudo cp moonraker.service /etc/systemd/system/

# Reload the unit definitions
sudo systemctl daemon-reload
```


## Start and manage services

### Start system units

Use `systemctl` as you normally would:

```bash
sudo systemctl start moonraker.service
sudo systemctl start klipper.service
sudo systemctl enable moonraker.service
sudo systemctl enable klipper.service
```

```bash
sudo systemctl status klipper.service
# sudo journalctl -xeu klipper.service -f
```

```bash
sudo systemctl status moonraker.service
```

### Start user units

The [klipper.target](./klipper.target) file can be used to check on and manage all services:

```bash
# List target units with "list-unit-files", which returns a tablular summary.
systemctl --user list-unit-files --all --with-dependencies klipper.target # This does not show their "status".

# Print full status of all services.
systemctl --user status --with-dependencies klipper.target

# Show status of the units (if they are loaded / "started").
systemctl --user list-units --all --with-dependencies klipper.target # This does not show unloaded units.

# Start, stop, and restart all services using the target
systemctl --user start klipper.target  # You may "start" with "restart" or "stop".
```

Example output when the target is loaded, but klipper was stopped:

```
$ systemctl --user list-units --all --with-dependencies klipper.target
 UNIT              LOAD   ACTIVE   SUB     DESCRIPTION                                             
 klipper.service   loaded inactive dead    Starts klipper on startup  
 moonraker.service loaded active   running API Server for Klipper  
 simulavr.service  loaded active   running Starts Simulavr for Klipper development and debugging   
 klipper.target    loaded active   active  Klipper software stack
```

You may also use standard systemctl commands to check on the services manually. For example:

```bash
# Check status
systemctl --user status klipper.service
systemctl --user status moonraker.service
systemctl --user status simulavr.service
systemctl --user status mainsail.service

# Start
systemctl --user start klipper.service
systemctl --user start moonraker.service
systemctl --user start simulavr.service
systemctl --user start mainsail.service
```

Most problems will show up there immediately.

The Mainsail unit should be installed system-wide, but it is not hard to run the command by hand when needed. Note that this Mainsail config uses the "hosts" network, which is not recommended for production (in that case, run Mainsail without Docker instead).

### Checking logs

For user services:

```bash
# Example for Mainsail
journalctl --user -xeu mainsail.service -f
```

For system services:

```bash
# Example for Moonraker
sudo journalctl -xeu moonraker.service -f
```

# Check that everything works

Check on moonraker's status by visitng: `http://YOURIPADDRESS:7125/`

![moonrakerstatus.png](images/moonrakerstatus.png)

Open the Mainsail app: `http://YOURIPADDRESS:8080/config`

![mainsailconfig.png](images/mainsailconfig.png)

If everything looks good, you can go on and configure Klipper to your liking.

For example, edit `mainsail.cfg` to adjust the path to the gcode files (e.g. from `~/printer_data/gcode` to `/path/to/klipper-stack/gcode`).

# Updating components with Moonraker

Setup "update_manager" entries at [moonraker.conf](./printer_data/config/moonraker.conf). 

Read the docs:

- https://github.com/Arksine/moonraker/blob/master/docs/configuration.md#update_manager
- https://docs.mainsail.xyz/setup/updates/update-manager
- https://github.com/Arksine/moonraker/blob/master/docs/configuration.md#git-repo-configuration

Example setup for simulavr's git repo: 

```yaml
# When defining a service, the "extension_name" must be the name of the systemd service.
[update_manager simulavr]
type: git_repo
channel: dev
path: /home/you/......./klipper-stack/simulavr
origin: git://git.savannah.nongnu.org/simulavr.git
primary_branch: master
managed_services: simulavr
```

Then add "simulavr" to the bottom of [moonraker.asvc](./printer_data/moonraker.asvc).

**Caveat for simulavr**: the upstream repo does not have any "tagged" versions, which means that Mainsail will not let you press the "update" button (it is grayed out :/). The only alternative is to fork it, add a tag, and replace the origin with yours. The Moonraker docs say:

> Moonraker can update repos without tags, however front ends may disable update controls when version information is not reported by Moonraker: https://moonraker.readthedocs.io/en/latest/configuration/#git-repo-configuration

# Run components manually

There is an old [startup.sh](./startup.sh) that can be used like this:

```bash
# Klipper group directory (i.e. to the parent directory of moonraker, klipper, mainsail, etc.).
export KLIPPER_GROUP=`pwd`

# Run all
source startup.sh
```

## Moonraker

1. Install moonraker with:

```bash
# Klipper group directory (i.e. to the parent directory of moonraker, klipper, mainsail, etc.).
export KLIPPER_GROUP=`pwd`

# NOTE: set "MOONRAKER_VENV" to where its virtual enviroment is.
# export MOONRAKER_VENV=${KLIPPER_GROUP}/moonraker/moonraker-env

# NOTE: set "DATA_PATH" to where the "printer_data" directory is.
# export DATA_PATH=${KLIPPER_GROUP}/printer_data

# Install moonraker
./moonraker/scripts/install-moonraker-arch.sh  # -z -x -d ${KLIPPER_GROUP}/printer_data
```

See details on the script's usage [here](https://moonraker.readthedocs.io/en/latest/installation/). The script [install-moonraker-arch.sh](./moonraker/scripts/install-moonraker-arch.sh) only uses "sudo" to install dependencies with pacman (and one from the AUR with yay).

2. Set `validate_service: False` in [moonraker.conf](./printer_data/config/moonraker.conf) (done by default).

This is how the included [moonraker.conf](./printer_data/config/moonraker.conf) starts:

```
# Moonraker Configuration File

[server]
host: 0.0.0.0
port: 7125
# Make sure the klippy_uds_address is correct.  It is initialized
# to the default address.
klippy_uds_address: /tmp/klippy_uds

[machine]
provider: systemd_cli
# https://moonraker.readthedocs.io/en/latest/configuration/#machine
validate_service: False

# ...
```

3. Setup and run Moonraker:
  - Klipper configuration files need to be in the `printer_data/config` directory, so that Mainsail can read them.
  - _Note_: `printer_data` **must not** be in the `moonraker` directory, otherwise Moonraker will deny access to Mainsail.
  - _Note_: Because klipper is started manually, these are not used unless pointed at.

```bash
# Klipper group directory (i.e. to the parent directory of moonraker, klipper, mainsail, etc.).
export KLIPPER_GROUP=`pwd`

# Activate the virtual environment (created by the install script).
source ${KLIPPER_GROUP}/moonraker-env/bin/activate

# Start moonraker
${KLIPPER_GROUP}/moonraker/moonraker/moonraker.py --datapath ${KLIPPER_GROUP}/printer_data/ --configfile ${KLIPPER_GROUP}/printer_data/config/moonraker.conf -n
```

## Klipper configuration

There are two "fast" initial choices, and the usual third one:

- Option 1: Install Klipper to any microcontroller, and configure its path at [printer_minimal.cfg](./printer_data/config/printer_minimal.cfg). It should just work.
- Option 2: Install `simulavr` and the required dependencies, and configure [printer_simulavr.cfg](./printer_data/config/printer_simulavr.cfg). See instructions in the "Simulavr setup" section below.
- Option 3: Install klipper on your machine's control board, and configure everything (the usual).

## Klippy

4. Open a new terminal and start Klipper. For install help see:
    - Original Mainsail docs: https://docs.mainsail.xyz/setup/manual-setup/klipper
    - _Note_: remember to have the `[virtual_sdcard]` setting in `printer.cfg` pointing to `printer_data/gcode`. This will make Mainsail happy.

```bash
# Klipper group directory (i.e. to the parent directory of moonraker, klipper, mainsail, etc.).
export KLIPPER_GROUP=`pwd`

# Optionally follow the log file in the background.
tail -f /tmp/klippy.log -n 0 &

# Run klippy!
python ${KLIPPER_GROUP}/klipper/klippy/klippy.py "${KLIPPER_GROUP}/printer_data/config/printer.cfg" -l /tmp/klippy.log -a /tmp/klippy_uds
```

5. Visit http://localhost:7125/ to check that Moonraker is working, and no warnings are shown.

## Mainsail

Docker is the most straightforward way of setting up mainsail.

A manual setup is also possible. Read on...

### Nginx

Official guide: https://docs.mainsail.xyz/setup/getting-started/manual-setup#mainsail

You'll need `nginx` on your system:

```bash
# Arch Linux
sudo pacman -S nginx

# Ubuntu / RPi OS
sudo apt install -y nginx
```

1. Follow [that guide](https://docs.mainsail.xyz/setup/getting-started/manual-setup#mainsail) up to, _but not including_, the "Install httpdocs" step.
3. Now edit `/etc/nginx/sites-available/mainsail` and change:
    - Optional: the port from `80` to `8080`.
    - The "web_path root" to `/path/to/your/klipper-stack/mainsail/mainsail` (adjust as needed).
4. Then download the pre-built website from upstream Mainsail, building it on the Pi  is cumbersome due to outdated system packages:

```bash
mkdir -p /path/to/your/klipper-stack/mainsail/mainsail
cd /path/to/your/klipper-stack/mainsail/mainsail
wget -q -O mainsail.zip https://github.com/mainsail-crew/mainsail/releases/latest/download/mainsail.zip && unzip mainsail.zip && rm mainsail.zip
```

Then restart nginx with `sudo systemctl restart nginx.service` and visit http://localhost:8080 to check if the site loads. If nothing else is running it may simply be a blank gray screen.

### Docker

> Note that `mainsail_config.json` may need to be created beforehand, otherwise docker creates it as a directory, and then fails to do the mount.

> Note that the container's IP address must be allowed in  [moonraker.conf](./printer_data/config/moonraker.conf), by adding `172.17.0.1/16` (to the `trusted_clients` list under `[authorization]`) or whatever IP docker is using in your system (I used `ip a` to find out, and tried the one from the network named `docker0`).

6. Run [Mainsail in Docker](https://docs.mainsail.xyz/setup/getting-started/docker); this is the least-effort setup by far:

```bash
# Klipper group directory (i.e. to the parent directory of moonraker, klipper, mainsail, etc.).
export KLIPPER_GROUP=`pwd`

# To remove a previous container:
sudo docker rm mainsail && echo "Removed Mainsail container." || echo "No container to remove."

# Alternative: using the host's network (more insecure).
sudo docker run --rm --name mainsail -v "${KLIPPER_GROUP}/printer_data/config/mainsail_config.json:/usr/share/nginx/html/config.json" --net="host" ghcr.io/mainsail-crew/mainsail

# To start the container normally.
# sudo docker run --name mainsail -v "${KLIPPER_GROUP}/printer_data/config/mainsail_config.json:/usr/share/nginx/html/config.json" -p "8080:80" ghcr.io/mainsail-crew/mainsail 
```

7. Visit http://localhost/ to open Mainsail.

Note that this Mainsail config uses the "hosts" network, which is not recommended for production (in that case, run Mainsail without Docker instead).

### Build Mainsail with npm

You can also install `npm` to build the site as per: https://github.com/mainsail-crew/mainsail/blob/develop/.github/workflows/build.yml

This is an alternative to downloading the pre-built website from upstream Mainsail.

```bash
sudo apt install npm

cd /home/pi/klipper-stack/mainsail/
npm ci
npm run build
 
mkdir mainsail
unzip dist/mainsail.zip -d mainsail/
```

As noted above, the Raspberry Pi uses an old version of npm, and will produce many warnings (maybe errors) and take a long time.

### Serve Mainsail with npm

See instructions above, at the "Mainsail development server" section.

## Troubleshooting

## General

If Klipper complains about not being able to connect to the MCU, first check your configuration (you should be loading Klipper configs from here: [simulavr](./printer_data/config/simulavr)) and then try repeating the build steps above.

## Klipper not flashing the Arduino UNO

Solution if the flashing fails with the following error on your Linux system:

```
Version: v0.2.1-98-g4b12b403
  Linking out/klipper.elf
/usr/lib/gcc/avr/5.4.0/../../../avr/bin/ld: out/klipper.elf section `.text' will not fit in region `text'
/usr/lib/gcc/avr/5.4.0/../../../avr/bin/ld: address 0x800b6e of out/klipper.elf section `.data' is not within region `data'
/usr/lib/gcc/avr/5.4.0/../../../avr/bin/ld: address 0x800cda of out/klipper.elf section `.bss' is not within region `data'
/usr/lib/gcc/avr/5.4.0/../../../avr/bin/ld: address 0x800b6e of out/klipper.elf section `.data' is not within region `data'
/usr/lib/gcc/avr/5.4.0/../../../avr/bin/ld: address 0x800cda of out/klipper.elf section `.bss' is not within region `data'
/usr/lib/gcc/avr/5.4.0/../../../avr/bin/ld: region `text' overflowed by 4534 bytes
collect2: error: ld returned 1 exit status
make: *** [Makefile:72: out/klipper.elf] Error 1
```

The gcc compiler on Bullseye makes a larger program that does not fit into the UNO. The latest gcc (May 2023) does not have this problem. This means that it is probably specific to the Raspberry Pi and/or Ubuntu.

To fix it follow [this guide](https://github.com/Klipper3d/klipper/issues/4938#issuecomment-1094246978) by GitHub user `orobardet`, copied here for reference:

1. Add the buster package source:
    - Edit the source file: sudo vi /etc/apt/sources.list
    - Add the following line just below the bullseye one: `deb http://raspbian.raspberrypi.org/raspbian/ buster main contrib non-free rpi`
    - Tells apt to use only buster for the AVR packages (and only those):
2. Create a new apt preferences file: `sudo vi /etc/apt/preferences.d/avr-buster`
3. With the following content:
```
Package: avr-libc avrdude binutils-avr gcc-avr
Pin: release n=buster
Pin-Priority: 1001
```
4. Make `apt` discover the buster packages: `sudo apt update`
5. Install the "avr" packages (even if they are already installed, the command is to force apt to downgrade them): `sudo apt install avr-libc avrdude binutils-avr gcc-avr`
    - `apt` should output a warning message for each package saying they are downgraded
6. Try again to compile Klipper for AVR 328p, it should work now (do a make clean before the make).


# Changes

To make this work with your own repos, repeat the modifications I did to moonraker:

1. Edit the new "klipper" section at [moonraker.conf](./printer_data/config/moonraker.conf).
    - _Note_: options in the section are used to replace the defaults at [base_config.py](./moonraker/moonraker/components/update_manager/base_config.py).
2. If you use yet another Moonraker fork, update the links at [base_config.py](./moonraker/moonraker/components/update_manager/base_config.py).

Current [moonraker.conf](./printer_data/config/moonraker.conf):

```
[server]
host = 0.0.0.0
port = 7125
klippy_uds_address = /tmp/klippy_uds

[machine]
provider = systemd_cli
validate_service = False

[authorization]
cors_domains = 
        https://my.mainsail.xyz
        http://my.mainsail.xyz
        http://*.local
        http://*.lan
        http://localhost
trusted_clients = 
        10.0.0.0/8
        127.0.0.0/8
        169.254.0.0/16
        172.16.0.0/12
        192.168.0.0/16
        FE80::/10
        ::1/128
        172.17.0.0/16  # This allows connections from Mainsail-in-docker.

[update_manager]
enable_auto_refresh = False
refresh_interval = 672
enable_system_updates = False
channel = dev

[update_manager klipper]
type = git_repo
channel = dev
origin = https://gitlab.com/pipettin-bot/forks/klipper.git
primary_branch = pipetting
host_repo = https://gitlab.com/pipettin-bot/forks/klipper
is_system_service = False
```

Current [base_config.py](./moonraker/moonraker/components/update_manager/base_config.py):

```python
BASE_CONFIG: Dict[str, Dict[str, str]] = {
    "moonraker": {
        "origin": "https://gitlab.com/pipettin-bot/forks/moonraker.git",
        "primary_branch": "pipetting",
        "requirements": "scripts/moonraker-requirements.txt",
        "venv_args": "-p python3",
        "install_script": "scripts/install-moonraker-arch.sh",
        "host_repo": "https://gitlab.com/pipettin-bot/forks/moonraker",
        "env": sys.executable,
        # NOTE: This will return the path to "moonraker/"
        "path": str(source_info.source_path()),
        "managed_services": "moonraker"
    },
    "klipper": {
        "moved_origin": "https://gitlab.com/pipettin-bot/forks/klipper.git",
        "origin": "https://gitlab.com/pipettin-bot/forks/klipper.git",
        "primary_branch": "pipetting",
        "requirements": "scripts/klippy-requirements.txt",
        "venv_args": "-p python3",
        "install_script": "scripts/install-octopi.sh",  # NOTE: not yet supported.
        "host_repo": "https://gitlab.com/pipettin-bot/forks/klipper",
        "managed_services": "klipper",
        # NOTE: This will return the path to "moonraker/../klipper"
        "path": str(source_info.source_path().parent) + "/klipper"
    }
}
```

---

![klipper_stack.svg](./docs/imgs/klipper_stack.svg)

> An artsy stack "logo". Seems right since Klipper is not an isolated project.
