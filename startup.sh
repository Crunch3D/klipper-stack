#!/bin/bash

# Terminate all child processes on "Ctrl+C".
# Source: https://stackoverflow.com/a/35660327/11524079
trap terminate SIGINT
terminate(){
    pkill -SIGINT -P $$
    exit
}

# Klipper group directory (i.e. to the parent directory of moonraker, klipper, mainsail, etc.).
export KLIPPER_GROUP=`pwd`

# Run moonraker
(source ${KLIPPER_GROUP}/moonraker-env/bin/activate && ${KLIPPER_GROUP}/moonraker/moonraker/moonraker.py --datapath ${KLIPPER_GROUP}/printer_data/ --configfile ${KLIPPER_GROUP}/printer_data/config/moonraker.conf -n) &

# Run mainsail
(cd ${KLIPPER_GROUP}/moonraker-env/bin/activate && npm run serve) &

# Run Klipper
(touch /tmp/klippy.log & tail -f /tmp/klippy.log -n 0) &
(python ${KLIPPER_GROUP}/klipper/klippy/klippy.py "${KLIPPER_GROUP}/printer_data/config/printer.cfg" -l /tmp/klippy.log -a /tmp/klippy_uds) &

# Wait for all child processes to close
wait
